package ru.ifmo.cs;

import ru.ifmo.cs.people.*;

import java.util.ArrayList;
import java.util.List;

public class Table {
    public static void main(String[] args) {
        List table = new ArrayList();
        Vegan vasya = new Vegan("Vasya");
        Vegetarian natasha = new Vegetarian("Natasha");
        Omnivorous semen = new Omnivorous("Semen");
        table.add(vasya); // Vegan
        table.add(natasha); // Vegetarian
        table.add(semen); // Omnivorous
        System.out.println(table);
    }
    // Я сажу всех за стол, но люди недовольны (кроме Семена), поскольку они кушают разную еду.
}