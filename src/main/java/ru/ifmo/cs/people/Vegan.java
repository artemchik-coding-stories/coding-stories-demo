package ru.ifmo.cs.people;

public class Vegan {
    String name;
    public Vegan(String name){
        this.name = name;
    }
    @Override
    public String toString() {
        return (name + " is Vegan");
    }
}
