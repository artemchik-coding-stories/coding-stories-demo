package ru.ifmo.cs.people;

public class Omnivorous {
    String name;
    public Omnivorous(String name){
        this.name = name;
    }
    @Override
    public String toString() {
        return (name + " is Omnivorous");
    }
}
