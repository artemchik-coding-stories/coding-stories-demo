package ru.ifmo.cs.people;

public class Vegetarian {
    String name;
    public Vegetarian(String name){
        this.name = name;
    }
    @Override
    public String toString() {
        return (name + " is Vegetarian");
    }
}
